//
//  AppConstant.swift
//  EPS
//
//  Created by mac on 10/01/2017.
//  Copyright © 2017 digitonics. All rights reserved.
//

import Foundation
import UIKit

// Fonts
let kFontName = "Raleway"
let kFontSizeTitle = 14

// ViewControllers

let kStartQualityAssuranceVC = "StartQualityAssuranceVC"
let kMainStoryBoard = "Main"
let kRecordAudioViewController = "RecordAudioViewController"

let kProgressDialogEnd = "endProgress"
let kAutomateTest = "automateTest"
let kAutomateTestDialog = "automateDialogs"

let kSaveCountryName = "currentCurrentry"

let kScreenBrigtness =  "ScreenBrightness"
let kMicrophone = "Microphone"
let kheadset = "Headset"
let kcamera = "Camera"
let kGyro =  "Gryo"
let kAcceleartion = "Accelerometer"
let kCarrier =  "Carrier"
let kWifi = "Wifi"
let kBluetooth =  "Bluetooth"
let kGPS =  "GPS"
let kFlashlight = "Flashlight"
let kviberaten =  "Vibration"
let kplayAutdio =  "PlayAudio"
let kVolumeUp = "Volume Up"
let kVolumeDown = "Volume Down"

// MARK: - Toches Feature
let kSingleTouch = "signleTouch"
let kMultipleTouch = "multipletouch"
let kPinchToZoom = "pinchZoom"
let kColorValue = "colorValue"

// Camera Feature
let kFrontCamera = "camerafront"
let kFrontVideo = "cameraVideofront"
let kBackVideo = "backvideo"
let kBackCamera = "backCamera"

let kTotalCounterCamera = "totalCamera"

let kInnerObjectArray = "innerObject"
let kTitleValueObject = "Title"
let kValueObject = "Value"

// MARK: - Automatic testing
let kAutoCarierSignal = "carier"
let kAutoWifi = "wifi"
let kAutoBluetooth = "bluetooth"
let kAutoGPS = "gps"
let kAutoFlashlight = "flaslght"
let kAutoPlayAudio = "playAudio"
let kAutoVibratn = "vibration"

// MARK: - Manual testing
let kManualScreenBrightness = "ScreenBrightness"
let kManualCamera = "Cameras"
let kManualTouch = "Touchs"
let kManualVolumeUp = "VolumeUp"
let kManualVolumeDown = "VolumeDown"
let kManualMicrophone = "Microphone"
let kManualHeadphone = "HeadPhone"

let kMultiTouch = "multiTouchs"
let kPinchTOZoom = "pinchtoZom"

let kEmptyString = "-1"

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}


extension UIScrollView {
    
    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x: 0, y: childStartPoint.y, width: 1, height: self.frame.height) , animated: true)
        }
    }
    
}

func saveUsrDefault(keyValue: String, valueIs: String)
{
    let defaults = UserDefaults.standard
    defaults.synchronize()
    UserDefaults.standard.synchronize()
    defaults.set(valueIs, forKey: keyValue)
}

func getValueForKey(keyValue: String) -> String
{
    let defaults = UserDefaults.standard
    defaults.synchronize()
    UserDefaults.standard.synchronize()
    
    let valueForStr = defaults.value(forKey: keyValue)
    if(valueForStr != nil)
    {
        return valueForStr as! String
    }
    else{
        return kEmptyString
    }
    
}

