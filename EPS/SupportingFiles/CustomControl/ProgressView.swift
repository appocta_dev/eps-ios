//
//  ProgressView.swift
//  CustomProgressBar
//
//  Created by Sztanyi Szabolcs on 16/10/14.
//  Copyright (c) 2014 Sztanyi Szabolcs. All rights reserved.
//

import UIKit

class ProgressView: UIView,CAAnimationDelegate {

    fileprivate let progressLayer: CAShapeLayer = CAShapeLayer()
    
    fileprivate var progressLabel: UILabel
    
    required init(coder aDecoder: NSCoder) {
        progressLabel = UILabel()
        super.init(coder: aDecoder)!
        createProgressLayer()
        createLabel()
    }
    
    override init(frame: CGRect) {
        progressLabel = UILabel()
        super.init(frame: frame)
        createProgressLayer()
        createLabel()
    }
    
    func createLabel() {
        progressLabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: frame.width, height: 60.0))
        progressLabel.textColor = .white
        progressLabel.textAlignment = .center
        progressLabel.font = UIFont(name: "HelveticaNeue-UltraLight", size: 40.0)
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(progressLabel)
        
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: progressLabel, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: progressLabel, attribute: .centerY, multiplier: 1.0, constant: 0.0))
    }
    
    fileprivate func createProgressLayer() {
        let startAngle = CGFloat(Double.pi)
        let endAngle = CGFloat(Double.pi * 2 + Double.pi)
        let centerPoint = CGPoint(x: frame.width/2 , y: frame.height/2)
        
        let gradientMaskLayer = gradientMask()
        progressLayer.path = UIBezierPath(arcCenter:centerPoint, radius: frame.width/2 - 80.0, startAngle:startAngle, endAngle:endAngle, clockwise: true).cgPath
        progressLayer.backgroundColor = UIColor.clear.cgColor
        progressLayer.fillColor = nil
        progressLayer.strokeColor = UIColor.black.cgColor
        progressLayer.lineWidth = 1.0
        progressLayer.strokeStart = 0.0
        progressLayer.strokeEnd = 0.0
        
        gradientMaskLayer.mask = progressLayer
        layer.addSublayer(gradientMaskLayer)
    }
    
    fileprivate func gradientMask() -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds

        gradientLayer.locations = [0.0, 1.0]
        
        let colorTop: AnyObject = UIColor(red: 45.0/255.0, green: 201.0/255.0, blue: 107.0/255.0, alpha: 1.0).cgColor
        let colorBottom: AnyObject = UIColor(red: 45.0/255.0, green: 201.0/255.0, blue: 107.0/255.0, alpha: 1.0).cgColor

        let arrayOfColors: [AnyObject] = [colorTop, colorBottom]
        
        gradientLayer.colors = arrayOfColors
        
        return gradientLayer
    }
    
    func hideProgressView() {
        progressLayer.strokeEnd = 0.0
        progressLayer.removeAllAnimations()
      //  progressLabel.text = "Load content"
    }
    
    func animateProgressView() {
      //  progressLabel.text = "Loading..."
        progressLayer.strokeEnd = 0.0
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = CGFloat(0.0)
        animation.toValue = CGFloat(1.0)
        animation.duration = 1.0
        animation.delegate = self
        animation.isRemovedOnCompletion = false
        animation.isAdditive = true
        animation.fillMode = kCAFillModeForwards
        
        progressLayer.add(animation, forKey: "strokeEnd")
    }
    
     func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
       // progressLabel.text = "Done"
        
        let defaults = UserDefaults.standard
        let automateText = defaults.object(forKey: kAutomateTest) as! String
        if(automateText == "1")
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kAutomateTestDialog), object: nil)
        }
        else if(automateText == "0")
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kProgressDialogEnd), object: nil)
        }
    }
}
