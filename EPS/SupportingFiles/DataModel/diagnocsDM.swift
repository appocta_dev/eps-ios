//
//  diagnocsDM.swift
//  EPS
//
//  Created by mac on 10/01/2017.
//  Copyright © 2017 digitonics. All rights reserved.
//

import UIKit

class diagnocsDM: NSObject {

    var titleOfItem: String = ""
    var imgName: String = ""
    var isSelected: Bool = false
    var percentage: String = ""
    var selectedIndex: Int = 0
    var selectedImg: String = ""
    
    init?(title: String, imgNames: String, isSelced:Bool, percentage:String, indexValue:Int, selectedImg:String)
    {
        // Initialize stored properties.
        self.titleOfItem = title
        self.imgName = imgNames
        self.isSelected = isSelced
        self.percentage = percentage
        self.selectedIndex = indexValue
        self.selectedImg = selectedImg
        
        if title.isEmpty || imgNames.isEmpty {
            return nil
        }
    }
}
