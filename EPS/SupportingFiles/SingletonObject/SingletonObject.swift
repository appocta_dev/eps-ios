//
//  SingletonObject.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/29/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit

class SingletonObject: NSObject {

    var cellOfObjects = [DiagnocItem]()
    var arrayOfObjects = [diagnocsDM]()
    
    var manualTitle = ""
    
    var indexValue = 0
    
    static let sharedInstance : SingletonObject = {
        let instance = SingletonObject()
        return instance
    }()
    
    var cellManalObjects = DiagnocItem()
    
    var isFillData = false
    
    convenience override init() {
        self.init(cellObjesTmp: [DiagnocItem](), tmpIndex: [diagnocsDM](), tmpDigitoni: DiagnocItem(), tmpFill: false, tmpTest: "", indexVa:0)
    }
    
    
    //MARK: Init String
    init( cellObjesTmp: [DiagnocItem], tmpIndex: [diagnocsDM], tmpDigitoni: DiagnocItem, tmpFill: Bool, tmpTest:String, indexVa: Int)
    {
        self.cellOfObjects = cellObjesTmp
        self.arrayOfObjects = tmpIndex
        self.cellManalObjects = tmpDigitoni
        self.isFillData = tmpFill
        self.manualTitle = tmpTest
        self.indexValue = indexVa
    }
}
