//
//  CameraViewController.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/30/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit
import MobileCoreServices


class CameraViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    var arrayOfDiagonocs = [diagnocsDM]()
    
    let kFrontVideoTitle = "Front Video"
    let kBackCameraTitle = "Back Camera"
    let kFrontCameraTitle = "Front Camera"
    let kBackVideoTitle = "Back Video"
    
    var currentTitle = ""
    
    var kFrontVideoTitleBool = false
    var kBackCameraTitleBool = false
    var kFrontCameraTitleBool = false
    var kBackVideoTitleBool = false
    
    var totalCurrentCounter = 0
    
    @IBOutlet weak var collectionViewCell: UICollectionView?
    let single = SingletonObject.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(buttonNotifition),
            name: NSNotification.Name(rawValue: kAutomateTestDialog),
            object: nil)
        
        relaodItems()
        //        ColorTabView().loadNib()
        
        let mailItem = UIButton.init(type: UIButtonType.custom)
        
        mailItem.setImage(UIImage.init(named: "backs"), for: UIControlState())
        mailItem.frame = CGRect.init(x: 0, y: 0, width: 25, height: 20)
        mailItem.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        mailItem.tag = 1
        
        let barButton = UIBarButtonItem.init(customView: mailItem)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.title = "Camera"
        // Do any additional setup after loading the view.

        if(getValueForKey(keyValue: kFrontVideo) == "0" || getValueForKey(keyValue: kFrontVideo) == "0%" || getValueForKey(keyValue: kFrontVideo) == "-1")
        {
            self.kFrontVideoTitleBool = false
        }
        else
        {
            self.kFrontVideoTitleBool = true
        }
        
        if(getValueForKey(keyValue: kBackCamera) == "0" || getValueForKey(keyValue: kBackCamera) == "0%" || getValueForKey(keyValue: kBackCamera) == "-1")
        {
            self.kBackCameraTitleBool = false
        }
        else
        {
            self.kBackCameraTitleBool = true
        }
        
        if(getValueForKey(keyValue: kFrontCamera) == "0" || getValueForKey(keyValue: kFrontCamera) == "0%" || getValueForKey(keyValue: kFrontCamera) == "-1")
        {
            self.kFrontCameraTitleBool = false
        }
        else
        {
            self.kFrontCameraTitleBool = true
        }
        
        
        if(getValueForKey(keyValue: kFrontVideo) == "0" || getValueForKey(keyValue: kFrontVideo) == "0%" || getValueForKey(keyValue: kFrontVideo) == "-1")
        {
            self.kBackVideoTitleBool = false
        }
        else
        {
            self.kBackVideoTitleBool = true
        }
    }
    
    func back()
    {
        self.single.isFillData = true
        loadDataView()
    }

    
    @objc func buttonNotifition(_ notification: Notification)
    {
        for i in 0 ..< self.single.arrayOfObjects.count
        {
            let indxValue = self.single.arrayOfObjects[i]
            
            let cellObject = self.single.cellOfObjects[i]
            if(indxValue.percentage == "100")
            {
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0xd92b2b)
                cellObject.circularview!.layer.borderColor = UIColor(netHex:0x1dca66).cgColor
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0x1dca66)
                
                cellObject.circularPercentage?.text = "100%"
            }
            else
            {
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0xd92b2b)
                
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0xd92b2b)
                cellObject.circularview!.layer.borderColor = UIColor(netHex:0xd92b2b).cgColor
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0xd92b2b)
                cellObject.collectionAnimated?.hideProgressView()
                
                cellObject.circularPercentage?.text = "0%"
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        
    }
    
    func relaodItems()
    {
        self.arrayOfDiagonocs.removeAll()
        
        self.single.arrayOfObjects.removeAll()
        self.single.cellOfObjects.removeAll()
        
        for i in 0 ..< 4
        {
            var imgName = ""
            var titleName = ""
            var isSelected = false
            var percentage = "0"
            var slectedInd = -1
            var selectedImg = ""
            
            switch i
            {
            case 0:
                let objectValue = getValueForKey(keyValue: kFrontCamera)
                if(objectValue == "1")
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "100"
                }
                else if(objectValue == "-1")
                {
                    isSelected = false
                    percentage = "-1"
                    percentage = "0"
                }
                else
                {
                    slectedInd = i
                    isSelected  = true
                    percentage = "0"
                }
                imgName = "camera-icon"
                titleName = kFrontCameraTitle
                selectedImg = "Camera-over"
                break
            case 1:
                imgName = "camera-icon"
                titleName = kBackCameraTitle
                selectedImg = "Camera-over"
                
                let objectValue = getValueForKey(keyValue: kBackCamera)
                if(objectValue == "1")
                {
                    isSelected = true
                    slectedInd = i
                    percentage = "100"
                }
                else if(objectValue == "-1")
                {
                    isSelected = false
                    percentage = "-1"
                    percentage = "0"
                }
                else
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "0"
                }
                
                break
            case 2:
                
                
                imgName = "video_off"
                titleName = kFrontVideoTitle
                selectedImg = "video_on"
                
                
                let objectValue = getValueForKey(keyValue: kFrontVideo)
                if(objectValue == "1")
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "100"
                }
                else if(objectValue == "-1")
                {
                    isSelected = false
                    percentage = "-1"
                    percentage = "0"
                }
                else
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "0"
                }
                break
                
            case 3:
                imgName = "video_off"
                titleName = kBackVideoTitle
                selectedImg = "video_on"
                
                let objectValue = getValueForKey(keyValue: kBackVideo)
                if(objectValue == "1")
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "100"
                }
                else if(objectValue == "-1")
                {
                    isSelected = false
                    percentage = "-1"
                    percentage = "0"
                }
                else
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "0"
                }
                break
                
            default:
                break
            }
            
            let mainOBject = diagnocsDM(title: titleName, imgNames: imgName, isSelced: isSelected, percentage: percentage, indexValue: slectedInd,selectedImg:selectedImg)
            self.arrayOfDiagonocs.insert(mainOBject!, at: i)
        }
        
        self.collectionViewCell?.delegate = self
        self.collectionViewCell?.dataSource = self
        self.collectionViewCell?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiagnocItem", for: indexPath) as! DiagnocItem
        
        cell.collectionCellImg!.layer.borderWidth = 1
        cell.collectionCellImg!.layer.masksToBounds = false
        cell.collectionCellImg!.layer.borderColor = UIColor(netHex:0x363a42).cgColor
        cell.collectionCellImg!.layer.cornerRadius = cell.collectionCellImg!.frame.height/2
        cell.collectionCellImg!.clipsToBounds = true
        
        let selectedColor = UIColor(netHex:0x349efb)
        cell.collectionCellImgInner!.layer.borderWidth = 1
        cell.collectionCellImgInner!.layer.masksToBounds = false
        cell.collectionCellImgInner!.layer.borderColor = UIColor(netHex:0x363a42).cgColor
        cell.collectionCellImgInner!.layer.cornerRadius = cell.collectionCellImgInner!.frame.height/2
        cell.collectionCellImgInner!.clipsToBounds = true
        cell.collectionCellImgInner?.backgroundColor = selectedColor
        
        let colorBar = UIColor(netHex:0xc6cbd8)
        cell.collectionCellTitle?.textColor = colorBar
        
        let currentOBject = arrayOfDiagonocs[indexPath.row]
        
        if(currentOBject.isSelected && currentOBject.selectedIndex == indexPath.row)
        {
            cell.circularview?.isHidden = false
            saveUsrDefault(keyValue: kAutomateTest, valueIs: "1")
            cell.collectionAnimated?.isHidden = false
            cell.collectionAnimated?.animateProgressView()
        }
        else{
            cell.circularview?.isHidden = true
            cell.collectionAnimated?.hideProgressView()
        }
        
        cell.circularPercentage?.text = currentOBject.percentage
        cell.collectionCellImgInner?.isHidden = true
        
        let image: UIImage = UIImage(named: currentOBject.imgName)!
        
        cell.collectionCellTitle?.text = currentOBject.titleOfItem
        cell.collectionCellIcon?.image = image
        
        cell.circularview!.layer.borderWidth = 1
        cell.circularview!.layer.masksToBounds = false
        
        cell.circularview!.layer.cornerRadius = cell.circularview!.frame.height/2
        cell.circularview!.clipsToBounds = true
        
        self.single.arrayOfObjects.append(currentOBject)
        self.single.cellOfObjects.append(cell)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let currentOBject = arrayOfDiagonocs[indexPath.row]
        self.currentTitle = currentOBject.titleOfItem
    
        let isCamera = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        if (isCamera == true)
        {
            if(currentOBject.titleOfItem == kFrontVideoTitle)
            {
                takePhoto(isBackCamera: true, isVideo: true)
            }
            else if(currentOBject.titleOfItem == kBackCameraTitle)
            {
                takePhoto(isBackCamera: false, isVideo: false)
            }
            else if(currentOBject.titleOfItem == kFrontCameraTitle)
            {
                takePhoto(isBackCamera: true, isVideo: false)
            }
            else if(currentOBject.titleOfItem == kBackVideoTitle)
            {
                takePhoto(isBackCamera: false, isVideo: true)
            }
        }
        else
        {
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayOfDiagonocs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    // MARK: - Camera
    func takePhoto(isBackCamera: Bool, isVideo: Bool)
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        
        if(isVideo == true)
        {
            imagePicker.mediaTypes = [kUTTypeMovie as String]
        }
        else
        {
            imagePicker.cameraCaptureMode = .photo
        }
        

        imagePicker.delegate =  self
        
        if(isBackCamera == true)
        {
            imagePicker.cameraDevice = .front
        }
        else{
            imagePicker.cameraDevice = .rear
        }
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        /* if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
         //      self.imgPhotocompain?.contentMode = .scaleAspectFit
         //self.imgPhotocompain?.image = pickedImage
         
         }*/
        self.dismiss(animated: true, completion: nil)

        CheckAndStatus()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionButtonFail()
    {
        saveUsrDefault(keyValue: kTotalCounterCamera, valueIs: "0")
        loadDataView()
    }
    
    func CheckAndStatus()
    {
        let message = "Is "+self.currentTitle + " is working fine ?"
        // Create the alert controller
        let alertController = UIAlertController(title: "Testing complete", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
           
            self.calcualteItems(isTrue:true)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            
            self.calcualteItems(isTrue:false)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func calcualteItems(isTrue: Bool)
    {
        var strigValue = ""
        if(isTrue == true)
        {
             strigValue = "1"
        }
        else{
            strigValue = "0"
        }
        
        if(self.currentTitle == self.kFrontVideoTitle)
        {
            self.kFrontVideoTitleBool = true
            saveUsrDefault(keyValue: kFrontVideo, valueIs: strigValue)
        }
        else if(self.currentTitle == self.kBackCameraTitle)
        {
            self.kBackCameraTitleBool = true
            saveUsrDefault(keyValue: kBackCamera, valueIs: strigValue)
        }
        else if(self.currentTitle == self.kFrontCameraTitle)
        {
            self.kFrontCameraTitleBool = true
            saveUsrDefault(keyValue: kFrontCamera, valueIs: strigValue)
        }
        else if(self.currentTitle == self.kBackVideoTitle)
        {
            self.kBackVideoTitleBool = true
            saveUsrDefault(keyValue: kBackVideo, valueIs: strigValue)
        }
       
        if(self.kFrontVideoTitleBool == true && self.kBackCameraTitleBool == true && self.kFrontCameraTitleBool == true && self.kBackVideoTitleBool == true)
        {
            let frontVid = Int(getValueForKey(keyValue: kFrontVideo))!
            let kBackCameras = Int(getValueForKey(keyValue: kBackCamera))!
            let frntCam = Int(getValueForKey(keyValue: kFrontCamera))!
            let bckVid = Int(getValueForKey(keyValue: kBackVideo))!
            
            let progessValue = CGFloat(frontVid+kBackCameras+frntCam+bckVid)
            let totalValue = CGFloat(4)
            
            
            let percentage = CGFloat(progessValue/totalValue)*100
            if(percentage > 90)
            {
                saveUsrDefault(keyValue: kManualCamera, valueIs: "100%")
                saveUsrDefault(keyValue: kTotalCounterCamera, valueIs: "1")
            }
            else{
                saveUsrDefault(keyValue: kManualCamera, valueIs: "0%")
                saveUsrDefault(keyValue: kTotalCounterCamera, valueIs: "0")
            }
            self.loadDataView()
        }
        
        
        
        self.relaodItems()
    }
    
    func loadDataView()
    {
        self.single.isFillData = true
        saveUsrDefault(keyValue: kAutomateTest, valueIs: "1")
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
