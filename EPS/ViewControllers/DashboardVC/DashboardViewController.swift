//
//  DashboardViewController.swift
//  EPS
//
//  Created by mac on 10/01/2017.
//  Copyright © 2017 digitonics. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

import CoreLocation
import PKHUD

class DashboardViewController: UIViewController,CLLocationManagerDelegate {

    let locationManager = CLLocationManager()

    @IBOutlet weak var lblTotalSize: UILabel?
    @IBOutlet weak var lblVersion: UILabel?
    @IBOutlet weak var lblBattery: UILabel?

    let progressHUD = ProgressHUD(text: "Saving Photo")

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.tintColor = UIColor.white;
        self.title = "DASHBOARD"
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: kFontName, size: CGFloat(kFontSizeTitle))!,  NSForegroundColorAttributeName: UIColor.white]

        // Do any additional setup after loading the view.
        
        getLocation()
        
        if let free = getTotalSize(){
            print(free) // free disk space, use as you like
            
            lblTotalSize?.text = String(free) + " GB"
        }

        let systemVersion = UIDevice.current.systemVersion
        let batteryLevel = UIDevice.current.batteryLevel

        let batLeft = batteryLevel * 100;
        let absoluteValue = abs(batLeft)
        
        lblVersion?.text = String(systemVersion)
        lblBattery?.text = String(absoluteValue) + " %"
        
        UIDevice.current.isBatteryMonitoringEnabled = true
       

        let hasLaunchedKey = "HasLaunched"
        let defaults = UserDefaults.standard
        let hasLaunched = defaults.bool(forKey: hasLaunchedKey)
        
        if !hasLaunched {
            defaults.set(true, forKey: hasLaunchedKey)
           self.callnextview()
        }
        

    }
    
    
    func getTotalSize() -> Int64?{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let dictionary = try? FileManager.default.attributesOfFileSystem(forPath: paths.last!) {
            if let freeSize = dictionary[FileAttributeKey.systemSize] as? NSNumber {
                let frezises = (freeSize.int64Value / 1073741824)
                return frezises
                //return freeSize.longLongValue
            }
        }else{
            print("Error Obtaining System Memory Info:")
        }
        return 0
    }

 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func getLocation() {
        guard CLLocationManager.locationServicesEnabled() else {
            print("Location services are disabled on your device. In order to use this app, go to " +
                "Settings → Privacy → Location Services and turn location services on.")
            return
        }
        
        let authStatus = CLLocationManager.authorizationStatus()
        guard authStatus == .authorizedWhenInUse else {
            switch authStatus {
            case .denied, .restricted:
                print("This app is not authorized to use your location. In order to use this app, " +
                    "go to Settings → GeoExample → Location and select the \"While Using " +
                    "the App\" setting.")
                
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
                
            default:
                print("Oops! Shouldn't have come this far.")
            }
            
            return
        }
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    
    // MARK: - CLLocationManagerDelegate methods
    
    // This is called if:
    // - the location manager is updating, and
    // - it was able to get the user's location.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       // let newLocation = locations.last!
       // print(newLocation)
        
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude:  (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!)
        
        geoCoder.reverseGeocodeLocation(location)
        {
            (placemarks, error) -> Void in
            
           
            let placeArray = placemarks as [CLPlacemark]!
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placeArray?[0]
            
           /* let countryName = "" as! String
            // Location name
            let locationName = placeMark.addressDictionary?["Name"] as? String
            
            
            // Street address
             let street = placeMark.addressDictionary?["Thoroughfare"] as? String
            */
            
            // City
            if placeMark != nil
            {
                var city = ""
                var country = ""
                let values = placeMark.addressDictionary?["City"]
                if values is NSNull || values == nil
                {
                    city = ""
                }
                else
                {
                    city = (placeMark.addressDictionary?["City"] as? String)!
                }
                
                // Zip code
                //            if let zip = placeMark.addressDictionary?["ZIP"] as? NSString
                //            {
                //                print(zip)
                //            }
                
                // Country
                if placeMark.addressDictionary?["Country"] is NSNull
                {
                    country = ""
                }
                else
                {
                    country = (placeMark.addressDictionary?["Country"] as? String)!
                }
                
                
                let finalString = NSString(format:"%@, %@", city, country)
                
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(finalString, forKey: kSaveCountryName)
                UserDefaults.standard.synchronize()
            }
            else
            {
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set("", forKey: kSaveCountryName)
                UserDefaults.standard.synchronize()
            }
            
        }

    }
    
    // This is called if:
    // - the location manager is updating, and
    // - it WASN'T able to get the user's location.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       // print("Error: \(error)")
    }
    

    @IBAction func actionButtonDownloadCSV(_ sender: UIButton)
    {
        let kBaseURL = "http://demo-appocta.com/eps/web.php?fun=get_mobile_config"
        let url:URL = URL(string: kBaseURL)!
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            guard let data = data, let _:URLResponse = response, error == nil else {
                print("error")
                return
            }
                        
            var dictonary:NSDictionary?

        
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as NSDictionary?
                
                let automaticArry = dictonary?["automatic"] as? NSArray
                for i in 0 ..< (automaticArry?.count)!
                {
                    let tempDic: NSDictionary = automaticArry![i] as! NSDictionary
                    let strMoodCatID = tempDic ["m_label"] as! String
                    let strMoodID = tempDic ["m_type"] as! String
                    
                    print(strMoodCatID)
                    print(strMoodID)
                }
                
                let manualArr = dictonary?["mannual"] as? NSArray

                for i in 0 ..< (manualArr?.count)!
                {
                    let tempDic: NSDictionary = manualArr![i] as! NSDictionary
                    let strMoodCatID = tempDic ["m_label"] as! String
                    let strMoodID = tempDic ["m_type"] as! String
                 
                    print(strMoodCatID)
                    print(strMoodID)
                }
                

               
            } catch let error as NSError {
                print(error)
            }
            
        }
        
        task.resume()

    }
    
    
    @IBAction func actionButtonStartDiagnosing(_ sender: UIButton)
    {
        callnextview()
    }
    
    
    func callnextview()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: kMainStoryBoard, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: kStartQualityAssuranceVC) as! StartQualityAssuranceVC
        
        self.navigationController?.pushViewController(nextViewController , animated: true)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
