//
//  DiagnocItem.swift
//  EPS
//
//  Created by mac on 10/01/2017.
//  Copyright © 2017 digitonics. All rights reserved.
//

import UIKit

class DiagnocItem: UICollectionViewCell {

    @IBOutlet weak var collectionCellIcon: UIImageView?
    @IBOutlet weak var collectionCellTitle: UILabel?
    @IBOutlet weak var collectionCellImg: UIImageView?
    
    @IBOutlet weak var collectionCellImgInner: UIImageView?
    
    @IBOutlet weak var collectionAnimated: ProgressView?
    
    @IBOutlet weak var circularview: UIView?
    @IBOutlet weak var circularPercentage: UILabel?

}
