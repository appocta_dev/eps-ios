//
//  RestultViewController.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/31/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit

protocol RestultViewControllerDelegate: class {
    
    /// Media Launched successfully on the cast device
    func callController()
}

class RestultViewController: UIViewController {

    let cellIdentifer = "CellRowsTableViewCell"
    
    @IBOutlet weak var tblviewResults: UITableView?
    @IBOutlet weak var segmentControl: UISegmentedControl?
    
    weak var delegateObject: RestultViewControllerDelegate?
    
    var totalArray = NSMutableArray()
    
    var isAutomatic = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nibName = UINib(nibName: "tableviewHeaderCell", bundle: nil)
        self.tblviewResults?.register(nibName, forHeaderFooterViewReuseIdentifier: "tableviewHeaderCell")

        self.title = "Results"
        let totalCounter = 4
        
        self.totalArray.removeAllObjects()
       
        loadAutomaticArray()
        
        // Do any additional setup after loading the view.
    }
    
    func loadAutomaticArray()
    {
        self.isAutomatic = true
        var myDict: NSDictionary?
        if let path = Bundle.main.path(forResource: "DiaDataModel", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        
        self.totalArray.removeAllObjects()
        let automaticarr = myDict!["automaticArray"] as! NSArray
        
        for i in 0 ..< automaticarr.count
        {
            let mainObject = automaticarr[i] as! NSDictionary
            
            let titleIs = mainObject["itemName"] as! String
            
            let dictnaObj = NSMutableDictionary()
            dictnaObj.setObject(titleIs, forKey: kTitleValueObject as NSCopying)
            
            switch titleIs
            {
            case "Wifi":
                let objec = getValueForKey(keyValue: kAutoWifi)
                dictnaObj.setObject(objec, forKey: kValueObject as NSCopying)
                break
                
            case "Carrier":
                let objec = getValueForKey(keyValue: kAutoCarierSignal)
                dictnaObj.setObject(objec, forKey: kValueObject as NSCopying)
                break
                
            case "Bluetooth":
                
                let objec = getValueForKey(keyValue: kAutoBluetooth)
                dictnaObj.setObject(objec, forKey: kValueObject as NSCopying)
                break;
                
            case "GPS":
                let objec = getValueForKey(keyValue: kAutoGPS)
                dictnaObj.setObject(objec, forKey: kValueObject as NSCopying)
                
                break;
                
            case "Flashlight":
                let objec = getValueForKey(keyValue: kAutoFlashlight)
                dictnaObj.setObject(objec, forKey: kValueObject as NSCopying)
                
                break;
            case "PlayAudio":
                let objec = getValueForKey(keyValue: kAutoPlayAudio)
                dictnaObj.setObject(objec, forKey: kValueObject as NSCopying)
                
                break;
            case "Vibration":
                let objec = getValueForKey(keyValue: kAutoVibratn)
                dictnaObj.setObject(objec, forKey: kValueObject as NSCopying)
                break;
            default:
                break
            }
            self.totalArray.add(dictnaObj)
        }
        
        self.tblviewResults?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func segmentedControlValueChanged(segment: UISegmentedControl)
    {
        if segment.selectedSegmentIndex == 0 // automatic
        {
            loadAutomaticArray()
        }
        else // manual
        {
            manualArray()
        }
    }
    
    func manualArray()
    {
        self.isAutomatic = false
        self.totalArray.removeAllObjects()
        var myDict: NSDictionary?
        if let path = Bundle.main.path(forResource: "DiaDataModel", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        
        let manualArray = myDict!["manualArray"] as! NSArray

        for  i in stride(from: 0, to: manualArray.count, by: 1)
        {
            print(i)
            let dictnaObj = NSMutableDictionary()
            dictnaObj.setObject("not tested", forKey: kValueObject as NSCopying)
            dictnaObj.setObject("Camera", forKey: kTitleValueObject as NSCopying)
            
            let innerArray = NSMutableArray()
            for j in stride(from: 0, to: 4, by: 1)
            {
                print(j)
                
                let dictnaObj = NSMutableDictionary()
                dictnaObj.setObject("not tested", forKey: kValueObject as NSCopying)
                dictnaObj.setObject("Back Camera", forKey: kTitleValueObject as NSCopying)
                
                innerArray.add(dictnaObj)
            }
            
            dictnaObj.setObject(innerArray, forKey:kInnerObjectArray as NSCopying)
            self.totalArray.add(dictnaObj)
        }
        
        self.tblviewResults?.reloadData()
    }
    
    @IBAction func actionButtonDone()
    {
        self.dismiss(animated: true, completion: nil)
        self.delegateObject?.callController()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RestultViewController: UITableViewDelegate, UITableViewDataSource
{
    // MARK: - Tableview Functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.totalArray.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(self.isAutomatic == true)
        {
           return 0
        }
        else
        {
            let mutableObjectArr = self.totalArray[section] as! NSMutableDictionary
            let countValue = mutableObjectArr[kInnerObjectArray] as! NSMutableArray
            return countValue.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer) as! CellRowsTableViewCell
        
        if(self.isAutomatic == false)
        {
            let tmpObject = self.totalArray[indexPath.section] as! NSMutableDictionary
            let objectInrr = tmpObject[kInnerObjectArray] as! NSMutableArray
            let mutableObjectArr = objectInrr[indexPath.row] as! NSMutableDictionary
            
            let titleString = mutableObjectArr.object(forKey: kTitleValueObject)
            cell.lblResultName?.text = titleString as? String
            
            let valueIs = mutableObjectArr.object(forKey: kValueObject)
            cell.lblResultValue?.text = valueIs as? String
        }
        else{
            
        }

        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 37
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = self.tblviewResults?.dequeueReusableHeaderFooterView(withIdentifier: "tableviewHeaderCell" ) as! tableviewHeaderCell
        
        let mutableObjectArr = self.totalArray[section] as! NSMutableDictionary
        
        let titleString = mutableObjectArr.object(forKey: kTitleValueObject)
        headerView.lblResultName?.text = titleString as? String
        
        let valueIs = mutableObjectArr.object(forKey: kValueObject)
        headerView.lblResultValue?.text = valueIs as? String
        
        headerView.backgroundColor = UIColor.gray
        
        return headerView
    }
}
