//
//  DashboardViewController.swift
//  EPS
//
//  Created by mac on 10/01/2017.
//  Copyright © 2017 digitonics. All rights reserved.
//

import UIKit
import CoreTelephony
import SystemConfiguration.CaptiveNetwork
import CoreBluetooth
import AVFoundation
import AudioToolbox
import CoreLocation
import CoreMotion

class StartQualityAssuranceVC: UIViewController,CBPeripheralManagerDelegate, CLLocationManagerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVAudioRecorderDelegate , AVAudioPlayerDelegate, RestultViewControllerDelegate
{
    var yourcurentLocation = ""
    @IBOutlet weak var collectionViewCell: UICollectionView?
    @IBOutlet weak var segmentBar: UISegmentedControl?

    var arrayOfDiagonocs = [diagnocsDM]()
    let locationManager = CLLocationManager()
    var motionManager: CMMotionManager!

    var audioPlayer : AVAudioPlayer!

    var colelctionViewobject = ProgressView()
    var arrayOfLines = [UIView]()
    var arrayOfImages = [UIImageView]()
    var arrayAutomateTst = [ProgressView]()
    var arraylabelOfCirularView = [UILabel]()
    
    var currentManualStatus = [String]()
    let mailArray = NSMutableArray()

    
    var selectedIndex: Int = -1
    var isManual: Bool = false
    
    var isRunning: Bool = false
    
    var automateCurentIndexTest = 0
    
    var statusOfTest = false
    
    var audioRecordingIndex = 0
    
    var recordingSession : AVAudioSession!
    var audioRecorder    :AVAudioRecorder!
    var settings         = [String : Int]()

    var timer = Timer()
    let notificationCenter = NotificationCenter.default

    var volumeValue = Float(0.0)

    let singtl = SingletonObject.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let colorBar = UIColor(netHex:0x349efb)

        self.navigationController?.navigationBar.tintColor = UIColor.white;
        self.navigationController?.navigationBar.barTintColor = colorBar
        
        self.singtl.isFillData = false
        self.title = "Diagnosing"
        
        self.navigationController?.navigationBar.backItem?.title = ""

        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: kFontName, size: CGFloat(kFontSizeTitle))!,  NSForegroundColorAttributeName: UIColor.white]

        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated:false);
        setItemsInCell(true)
        self.collectionViewCell?.reloadData()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(buttonNotifition),
            name: NSNotification.Name(rawValue: kAutomateTestDialog),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(progressEnd),
            name: NSNotification.Name(rawValue: kProgressDialogEnd),
            object: nil)
        
        
        segmentBar?.isEnabled = false
        arrayAutomateTst.removeAll()
        
        self.navigationController?.navigationBar.backItem?.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if(self.singtl.isFillData == true)
        {
            self.singtl.cellManalObjects.collectionAnimated?.isHidden = false;
            self.singtl.cellManalObjects.collectionAnimated?.animateProgressView()
            
            manuAlTesting(self.singtl.manualTitle)
        }
    }
    
    @objc func progressEnd(_ notification: Notification)
    {
       // manuAlTesting(self.singtl.manualTitle)
    }

    func manuAlTesting(_ titleOfItem: String)
    {
        var values = ""
        switch titleOfItem
        {
        case "ScreenBrightness":
            values = getValueForKey(keyValue: kColorValue)
            break
        case "Camera":
            values = getValueForKey(keyValue: kManualCamera)
            break
        case "Touch":
            
            let pinchZoom = Int(getValueForKey(keyValue: kPinchToZoom))
            let multiplcCTOuch = Int(getValueForKey(keyValue: kMultipleTouch))
            let singltTouch = Int(getValueForKey(keyValue: kSingleTouch))
            
            let totalValue = CGFloat(pinchZoom!) + CGFloat(multiplcCTOuch!) + CGFloat(singltTouch!)
            let toalPerce = (totalValue/3)*100
            if(toalPerce > 80)
            {
                values = "1"
            }
            else
            {
                values = "0"
            }
            
            break
        case "Volume Up":
            values = getValueForKey(keyValue: kManualVolumeUp)
            break
        case "Volume Down":
            values = getValueForKey(keyValue: kManualVolumeDown)
            break
        case "Microphone":
            values = getValueForKey(keyValue: kManualMicrophone)
            break
        case "Head Phone":
            values = getValueForKey(keyValue: kManualHeadphone)
            break
       
        default:
            break
        }
        
        if(values == "0" || values == "0%")
        {
            self.statusOfTest = false
        }
        else
        {
            self.statusOfTest = true
        }
        
        
        
        let vw = arrayOfLines[self.singtl.indexValue]
        vw.isHidden = false
        
        let currentOBject = arrayOfDiagonocs[self.singtl.indexValue]
        let image: UIImage = UIImage(named: currentOBject.selectedImg)!
        let imgIcon = arrayOfImages[self.singtl.indexValue]
        
        let progessBar = arrayAutomateTst[self.singtl.indexValue]
        let lablelTxt = arraylabelOfCirularView[self.singtl.indexValue]
        let changeColorOfBg = arrayOfLines[self.singtl.indexValue]
        
        var ObjectsAre = ""
        var message = ""
        
        if(self.statusOfTest)
        {
            progessBar.isHidden = false
            imgIcon.image = image
            
            UserDefaults.standard.synchronize()
            
            if(currentOBject.titleOfItem == kGPS)
            {
                let locationName = UserDefaults.standard.object(forKey: kSaveCountryName)
                
                if locationName  == nil
                {
                    message = "App can't get your location"
                    lablelTxt.text = "0%"
                    ObjectsAre = currentOBject.titleOfItem + "="+"0&"
                    changeColorOfBg.backgroundColor = UIColor(netHex: 0xd92b2b)
                    progessBar.hideProgressView()
                    saveUsrDefault(keyValue: kAutoGPS, valueIs: "0%")
                }
                else
                {
                    lablelTxt.text = "100%"
                    ObjectsAre = currentOBject.titleOfItem + "="+"1&"
                    
                    let locationName = UserDefaults.standard.object(forKey: kSaveCountryName) as! NSString
                    
                    changeColorOfBg.backgroundColor = UIColor(netHex: 0x1dca66)
                    saveUsrDefault(keyValue: kAutoGPS, valueIs: "100%")
                    message = NSString(format:"Your current location is %@", locationName) as String
                }
            }
            else{
                message = currentOBject.titleOfItem + " is working fine"
            }
        }
        else{
            lablelTxt.text = "0%"
            progessBar.isHidden = false
            progessBar.hideProgressView()
            changeColorOfBg.backgroundColor = UIColor(netHex: 0xd92b2b)
            
            ObjectsAre = currentOBject.titleOfItem + "="+"0&"
            
            message = currentOBject.titleOfItem + " is not working fine"
        }
        
        if(currentOBject.titleOfItem == "Results")
        {
            progessBar.hideProgressView()
            lablelTxt.text = ""
            progessBar.isHidden = true
        }
        
        self.setObjectValue(valueStr: currentOBject.titleOfItem)
        
        showToast(message: message)
        mailArray.add(ObjectsAre)
        
        isRunning = false
    }
    
    @objc func buttonNotifition(_ notification: Notification)
    {
        let isIndexValid = arrayOfLines.indices.contains(automateCurentIndexTest)
        if(isIndexValid == true)
        {
            if(arrayOfDiagonocs.count == automateCurentIndexTest )
            {
                return
            }
            
            let vw = arrayOfLines[automateCurentIndexTest]
            vw.isHidden = false
            
            isRunning = false
            
            let currentOBject = arrayOfDiagonocs[automateCurentIndexTest]
            let image: UIImage = UIImage(named: currentOBject.selectedImg)!
            let imgIcon = arrayOfImages[automateCurentIndexTest]
            
            let progessBar = arrayAutomateTst[automateCurentIndexTest]
            let lablelTxt = arraylabelOfCirularView[automateCurentIndexTest]
            let changeColorOfBg = arrayOfLines[automateCurentIndexTest]
            
            var ObjectsAre = ""
            var message = ""
            
            if(self.statusOfTest)
            {
                progessBar.isHidden = false
                imgIcon.image = image
                
                UserDefaults.standard.synchronize()
                
                if(currentOBject.titleOfItem == kGPS)
                {
                    let locationName = UserDefaults.standard.object(forKey: kSaveCountryName)
                    
                    if locationName  == nil
                    {
                        message = "App can't get your location"
                        lablelTxt.text = "0%"
                        ObjectsAre = currentOBject.titleOfItem + "="+"0&"
                        changeColorOfBg.backgroundColor = UIColor(netHex: 0xd92b2b)
                        progessBar.hideProgressView()
                        saveUsrDefault(keyValue: kAutoGPS, valueIs: "0%")
                    }
                    else
                    {
                        lablelTxt.text = "100%"
                        ObjectsAre = currentOBject.titleOfItem + "="+"1&"
                        
                        let locationName = UserDefaults.standard.object(forKey: kSaveCountryName) as! NSString
                        
                        changeColorOfBg.backgroundColor = UIColor(netHex: 0x1dca66)
                        saveUsrDefault(keyValue: kAutoGPS, valueIs: "100%")
                        message = NSString(format:"Your current location is %@", locationName) as String
                    }
                }
                else{
                    message = currentOBject.titleOfItem + " is working fine"
                }
            }
            else{
                lablelTxt.text = "0%"
                progessBar.isHidden = false
                progessBar.hideProgressView()
                changeColorOfBg.backgroundColor = UIColor(netHex: 0xd92b2b)
                
                ObjectsAre = currentOBject.titleOfItem + "="+"0&"
                
                message = currentOBject.titleOfItem + " is not working fine"
            }
            
            self.setObjectValue(valueStr: currentOBject.titleOfItem)
            
            if(currentOBject.titleOfItem == "Results")
            {
                progessBar.hideProgressView()
                lablelTxt.text = ""
                progessBar.isHidden = true
                message = ""
            }
            
            showToast(message: message)
            mailArray.add(ObjectsAre)
            automateCurentIndexTest += 1
            
            
            
            
            if(self.timer.isValid)
            {
                self.timer.invalidate()
            }
            self.timer = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(self.automateTest), userInfo: nil, repeats: false)

            //self .perform(#selector(automateTest), with: nil, afterDelay: 0.6)
        }
    }
    
    func setObjectValue(valueStr: String)
    {
        switch valueStr
        {
            case "Wifi":
                if(self.statusOfTest)
                {
                    saveUsrDefault(keyValue: kAutoWifi, valueIs: "100%")
                }
                else
                {
                    saveUsrDefault(keyValue: kAutoWifi, valueIs: "0%")
                }
            break
            
        case "Carrier":
            if(self.statusOfTest)
            {
                saveUsrDefault(keyValue: kAutoCarierSignal, valueIs: "100%")
            }
            else
            {
                saveUsrDefault(keyValue: kAutoCarierSignal, valueIs: "0%")
            }
            break
            
        case "Bluetooth":
            if(self.statusOfTest)
            {
                saveUsrDefault(keyValue: kAutoBluetooth, valueIs: "100%")
            }
            else
            {
                saveUsrDefault(keyValue: kAutoBluetooth, valueIs: "0%")
            }
            break;
            
        case "GPS":
            if(self.statusOfTest)
            {
                saveUsrDefault(keyValue: kAutoGPS, valueIs: "100%")
            }
            else
            {
                saveUsrDefault(keyValue: kAutoGPS, valueIs: "0%")
            }
            break;
            
        case "Flashlight":
            if(self.statusOfTest)
            {
                saveUsrDefault(keyValue: kAutoFlashlight, valueIs: "100%")
            }
            else
            {
                saveUsrDefault(keyValue: kAutoFlashlight, valueIs: "0%")
            }
            break;
        case "PlayAudio":
            if(self.statusOfTest)
            {
                saveUsrDefault(keyValue: kAutoPlayAudio, valueIs: "100%")
            }
            else
            {
                saveUsrDefault(keyValue: kAutoPlayAudio, valueIs: "0%")
            }
            break;
        case "Vibration":
            if(self.statusOfTest)
            {
                saveUsrDefault(keyValue: kAutoVibratn, valueIs: "100%")
            }
            else
            {
                saveUsrDefault(keyValue: kAutoVibratn, valueIs: "100%")
            }
            break;
        default:
            break
        }
        
        
    }
    
    @objc func batteryLevelChanged(_ notification: Notification){
        //do stuff
        
        let currentOBject = arrayOfDiagonocs[selectedIndex]
        if(currentOBject.isSelected)
        {
            currentOBject.isSelected = false
        }
        else{
            currentOBject.isSelected = true
        }
        
        currentOBject.selectedIndex = selectedIndex
        
        let vw = arrayOfLines[selectedIndex] 
        vw.isHidden = false
        
        isRunning = false
        
        segmentBar?.isEnabled = true
        let image: UIImage = UIImage(named: currentOBject.selectedImg)!
        let imgIcon = arrayOfImages[selectedIndex]
        
        let progessBar = arrayAutomateTst[selectedIndex]
        let lablelTxt = arraylabelOfCirularView[selectedIndex]
        let changeColorOfBg = arrayOfLines[selectedIndex]
        
        var message = ""
        if(statusOfTest)
        {
            lablelTxt.text = "100%"
            progessBar.isHidden = false
            imgIcon.image = image
            changeColorOfBg.backgroundColor = UIColor(netHex: 0x1dca66)
            
            message = currentOBject.titleOfItem + " is working fine"
        }
        else{
            lablelTxt.text = "0%"
            progessBar.isHidden = false
            progessBar.hideProgressView()
            changeColorOfBg.backgroundColor = UIColor(netHex: 0xd92b2b)
            
            message = currentOBject.titleOfItem + " is not working fine"
        }
        
        showToast(message: message)
    }
    
    func showToast(message:String)
    {
        self.navigationController?.view.makeToast(message, duration: 1.0, position: .bottom)
    }
    
    func setItemsInCell(_ isManualTrue:Bool)
    {
        var myDict: NSDictionary?
        if let path = Bundle.main.path(forResource: "DiaDataModel", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }

        let automaticarr = myDict!["automaticArray"] as! NSArray
        let manualArray = myDict!["manualArray"] as! NSArray
        
        self.arrayOfDiagonocs.removeAll()
        var isAutoOn = false
        if(isManualTrue)
        {
            for i in 0 ..< automaticarr.count
            {
                let mainObject = automaticarr[i] as! NSDictionary
                
                let img = mainObject["itemIcon"] as! String
                let titleIs = mainObject["itemName"] as! String
                let isSelected = false
                let percentage = "0"
                let slectedInd = -1
                let selectedImg = mainObject["selectedIcon"] as! String
                
                let mainOBject = diagnocsDM(title: titleIs, imgNames: img, isSelced: isSelected, percentage: percentage, indexValue: slectedInd,selectedImg:selectedImg)
                self.arrayOfDiagonocs.insert(mainOBject!, at: i)
            }
            isAutoOn = true
        }
        else
        {
            for i in 0 ..< manualArray.count
            {
                let mainObject = manualArray[i] as! NSDictionary
                
                let img = mainObject["itemIcon"] as! String
                let titleIs = mainObject["itemName"] as! String
                let isSelected = false
                let percentage = "0"
                let slectedInd = -1
                let selectedImg = mainObject["selectedIcon"] as! String

                let mainOBject = diagnocsDM(title: titleIs, imgNames: img, isSelced: isSelected, percentage: percentage, indexValue: slectedInd,selectedImg:selectedImg)
                self.arrayOfDiagonocs.insert(mainOBject!, at: i)
            }
            isAutoOn = false
        }
        
        self.collectionViewCell?.reloadData()
        
        if(isAutoOn)
        {
            segmentBar?.isEnabled = false
            arrayAutomateTst.removeAll()
            self .perform(#selector(callautomation), with: nil, afterDelay: 0.6)
        }
        else{
            segmentBar?.isEnabled = true
            let defaults = UserDefaults.standard
            defaults.set("0", forKey: kAutomateTest)
        }
    }
    func callautomation()
    {
        let defaults = UserDefaults.standard
        defaults.set("1", forKey: kAutomateTest)
        
        automateCurentIndexTest = 0;
        automateTest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - COllectionView
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayOfDiagonocs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiagnocItem", for: indexPath) as! DiagnocItem

        cell.collectionCellImg!.layer.borderWidth = 1
        cell.collectionCellImg!.layer.masksToBounds = false
        cell.collectionCellImg!.layer.borderColor = UIColor(netHex:0x363a42).cgColor
        cell.collectionCellImg!.layer.cornerRadius = cell.collectionCellImg!.frame.height/2
        cell.collectionCellImg!.clipsToBounds = true

        let selectedColor = UIColor(netHex:0x349efb)
        cell.collectionCellImgInner!.layer.borderWidth = 1
        cell.collectionCellImgInner!.layer.masksToBounds = false
        cell.collectionCellImgInner!.layer.borderColor = UIColor(netHex:0x363a42).cgColor
        cell.collectionCellImgInner!.layer.cornerRadius = cell.collectionCellImgInner!.frame.height/2
        cell.collectionCellImgInner!.clipsToBounds = true
        cell.collectionCellImgInner?.backgroundColor = selectedColor

        let colorBar = UIColor(netHex:0xc6cbd8)
        cell.collectionCellTitle?.textColor = colorBar
        
        let currentOBject = arrayOfDiagonocs[indexPath.row]
        if(currentOBject.isSelected && currentOBject.selectedIndex == indexPath.row)
        {
            cell.circularview?.isHidden = false
            cell.collectionAnimated?.isHidden = false
        }
        else{
            cell.circularview?.isHidden = true
            cell.collectionAnimated?.hideProgressView()
        }
      
        arraylabelOfCirularView.insert(cell.circularPercentage!, at: indexPath.row)
        
        cell.collectionCellImgInner?.isHidden = true
        
        let image: UIImage = UIImage(named: currentOBject.imgName)!

        cell.collectionCellTitle?.text = currentOBject.titleOfItem
        cell.collectionCellIcon?.image = image
        
        cell.circularview!.layer.borderWidth = 1
        cell.circularview!.layer.masksToBounds = false
        cell.circularview!.layer.borderColor = UIColor(netHex:0x363a42).cgColor
        cell.circularview!.layer.cornerRadius = cell.circularview!.frame.height/2
        cell.circularview!.clipsToBounds = true
        
        arrayOfImages.insert(cell.collectionCellIcon!, at: indexPath.row)
        arrayOfLines.insert(cell.circularview!, at: indexPath.row)

        arrayAutomateTst.insert(cell.collectionAnimated!, at: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath)
    {
        print("User tapped on item \(indexPath.row)")
        
        if(isRunning == false)
        {
            //isRunning = true
            if(isManual)
            {
                let currentOBject = arrayOfDiagonocs[indexPath.row]

                let cell = collectionView.cellForItem(at: indexPath) as! DiagnocItem
                self.singtl.cellManalObjects  = cell
                self.singtl.manualTitle = currentOBject.titleOfItem
                self.singtl.indexValue = indexPath.row
                
                selectedIndex = indexPath.row
                segmentBar?.isEnabled = false
                cell.circularview?.isHidden = true

                if currentOBject.titleOfItem == kcamera
                {
//                    UserDefaults.standard.synchronize()
//                    UserDefaults.standard.set("0", forKey: kAutomateTest)
//                    UserDefaults.standard.synchronize()
                    
                    let viewController = UIStoryboard(name:"touchSBoard", bundle:nil).instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else if currentOBject.titleOfItem == kMicrophone
                {
                    
                    colelctionViewobject = cell.collectionAnimated!
                    cell.collectionAnimated?.isHidden = false;

                    self.showToast(message: "Your recording is start and will stop after 10 seconds!")
                    
                    self .perform(#selector(stopRecord), with: nil, afterDelay: 10.0)
                    setUpAudioRecorder()
                }
                else if currentOBject.titleOfItem == kviberaten
                {
                    colelctionViewobject = cell.collectionAnimated!
                    cell.collectionAnimated?.isHidden = false;
                    viberatePhone()
                }
                
                else if currentOBject.titleOfItem == kVolumeUp || currentOBject.titleOfItem == kVolumeDown
                {
                    self.listenVolumeButton()
                }
                    
                else if currentOBject.titleOfItem == "Touch"
                {
//                    UserDefaults.standard.synchronize()
//                    UserDefaults.standard.set("0", forKey: kAutomateTest)
//                    UserDefaults.standard.synchronize()
                    
                    let viewController = UIStoryboard(name:"touchSBoard", bundle:nil).instantiateViewController(withIdentifier: "TouchViewController") as! TouchViewController
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else  if currentOBject.titleOfItem == "ScreenBrightness"
                {
                    //                    UserDefaults.standard.synchronize()
                    //                    UserDefaults.standard.set("0", forKey: kAutomateTest)
                    //                    UserDefaults.standard.synchronize()

                    
                    let viewController = UIStoryboard(name:"touchSBoard", bundle:nil).instantiateViewController(withIdentifier: "ColorViewController") as! ColorViewController
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else if currentOBject.titleOfItem == "Results"
                {
                    self.segmentBar?.isEnabled = true
                    isRunning = false
                    self.loadResult(isShow: false)
                }
                else{
                    if(currentOBject.titleOfItem == "Microphone")
                    {
                        audioRecordingIndex = indexPath.row
                    }
                    automateCurentIndexTest = indexPath.row
                    callManualTest(currentOBject.titleOfItem)
                    
                    cell.collectionAnimated?.isHidden = false;
                    cell.collectionAnimated?.animateProgressView()
                    
                    let curnetIndx = String(indexPath.row)
                    if(currentManualStatus.contains(curnetIndx))
                    {
                        currentManualStatus.insert(curnetIndx, at: indexPath.row)
                    }
                }
            }
        }
    }
    
    // MARK :- manual Test
    func callManualTest(_ titleOfItem: String)
    {
        switch titleOfItem
        {
        case kScreenBrigtness:
            setBrightness()
            break
            
        case kcamera:
            let isCamera = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            if (isCamera == true)
            {
                checkCamera()
            }
            else{
                statusOfTest = false
            }
            break
            
        case kMicrophone:
//            let storyBoard : UIStoryboard = UIStoryboard(name: kMainStoryBoard, bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: kRecordAudioViewController) as! RecordAudioViewController
//            
//            self.navigationController?.pushViewController(nextViewController , animated: true)

            self .perform(#selector(stopRecord), with: nil, afterDelay: 10.0)

            setUpAudioRecorder()
            break
            
        case kheadset:
            headsetPlugIn()
            break
            
        case kGyro:
            motionManager = CMMotionManager()
            motionManager.startAccelerometerUpdates()
            if motionManager.isDeviceMotionAvailable
            {
                motionManager.startGyroUpdates()
                motionManager.stopGyroUpdates()
                statusOfTest = true
            }
            else{
                statusOfTest = false
            }
            break
            
        case kAcceleartion:
            if motionManager != nil
            {
                if motionManager.isAccelerometerAvailable
                {
                    statusOfTest = true
                    motionManager.accelerometerUpdateInterval = 0.01
                }
                else{
                    statusOfTest = false
                }
            }
            else{
                statusOfTest = false
            }
            break
        default:
            break
        }

        
//        if(currentManualStatus.count == arrayOfDiagonocs.count)
//        {
//            setReportBar()
//        }
    }
    
    func getInputFromUser(testingResult:String)
    {
        let message = "Is "+testingResult + " is working fine ?"
        // Create the alert controller
        let alertController = UIAlertController(title: "Testing complete", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.statusOfTest = true
//            self.checkResult()
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            self.statusOfTest = false
            
//            self.checkResult()
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
       
    }
    
    func checkResult()
    {
//        if(arrayOfDiagonocs.count == automateCurentIndexTest )
//        {
//            return
//        }
        self.colelctionViewobject.animateProgressView()
        
        let curnetIndx = String(selectedIndex)
        if(currentManualStatus.contains(curnetIndx))
        {
            currentManualStatus.insert(curnetIndx, at: selectedIndex)
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.automateTest), userInfo: nil, repeats: false)
    }
    
    func checkCamera()
    {
        let isCamera = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)

        if (isCamera == true)
        {
            // 1
            let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            // 2
            let deleteAction = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.takePhoto()
                
            })
            
            optionMenu.addAction(deleteAction)
            
            self.statusOfTest = true
            self.present(optionMenu, animated: true, completion: nil)
        }
        else{
         
            self.statusOfTest = false
        }
        
    }
    
    func takePhoto()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        imagePicker.delegate =  self
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
       /* if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //      self.imgPhotocompain?.contentMode = .scaleAspectFit
            //self.imgPhotocompain?.image = pickedImage
            
        }*/
        self.dismiss(animated: true, completion: nil)
        
        self.getInputFromUser(testingResult: kcamera)

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: nil)
        self.getInputFromUser(testingResult: kcamera)

    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    
    @IBAction func actionButtonSegmentConrol(_ sender: UISegmentedControl)
    {
      /*  arrayOfLines.removeAll()
        arrayOfDiagonocs.removeAll()
        arrayOfLines.removeAll()
        arraylabelOfCirularView.removeAll()
        
        setReset()
        if(sender.selectedSegmentIndex == 0)
        {
            // automatic
            isManual = false
            setItemsInCell(true)
        }
        else
        {
            // manual
            setReportBar()
            isManual = true
            setItemsInCell(false)
            isRunning = false
        }*/
        
        if(sender.selectedSegmentIndex == 0)
        {
            self.loadResult(isShow: true)
            //loadAutoMaticOrManual(isAutoMti: true)
        }
        else
        {
            loadAutoMaticOrManual(isAutoMti: false)
        }
    }
    
    func loadAutoMaticOrManual(isAutoMti: Bool)
    {
        arrayOfLines.removeAll()
        arrayOfDiagonocs.removeAll()
        arrayOfLines.removeAll()
        arraylabelOfCirularView.removeAll()
        
        setReset()
        if(isAutoMti == true)
        {
            // automatic
            isManual = false 
            setItemsInCell(true)
        }
        else
        {
            // manual
            setReportBar()
            isManual = true
            setItemsInCell(false)
            isRunning = false
        }
    }
    
    func automateTest()
    {
        if(automateCurentIndexTest < arrayAutomateTst.count)
        {
            let currentOBject = arrayAutomateTst[automateCurentIndexTest]

            if(arrayOfDiagonocs.count == automateCurentIndexTest )
            {
                currentOBject.hideProgressView()
                return
            }
            else
            {
                currentOBject.animateProgressView()
            }

            let crntObjects = arrayOfDiagonocs[automateCurentIndexTest]
            switch crntObjects.titleOfItem
            {
            case kCarrier:
                self.statusOfTest =  getCarrierName()
                break
                
            case kWifi:
                self.statusOfTest =  fetchSSIDInfo()
                break
                
            case kBluetooth:
                getBlueetoth()
                break
                
            case kGPS:
                if(CLLocationManager.locationServicesEnabled())
                {
                    getLocation()
                }
                else{
                    self.statusOfTest = false
                }
                break
                
            case kFlashlight:
                toggleFlash()
                break
            
            case kviberaten:
                viberatePhone()
                break
            
            case kplayAutdio:
                playSound()
                break
            default:
                break
            }
            isRunning = true

        }
        else{
            segmentBar?.isEnabled = true
            setReportBar()
            
            
            // setg
            isManual = true
            setItemsInCell(false)
            isRunning = false
            
            segmentBar?.selectedSegmentIndex = 1
            
        }
    }
    
 
    func setReportBar()
    {
        let mailItem = UIButton.init(type: UIButtonType.custom)
        
        mailItem.setImage(UIImage.init(named: "Mail-icon"), for: UIControlState())
        mailItem.frame = CGRect.init(x: 0, y: 0, width: 25, height: 20)
        mailItem.addTarget(self, action: #selector(StartQualityAssuranceVC.mailIcon(_:)), for: UIControlEvents.touchUpInside)
        mailItem.tag = 1

        let barButton = UIBarButtonItem.init(customView: mailItem)
        self.navigationItem.rightBarButtonItem = barButton
        
        
        let leftItems = UIButton.init(type: UIButtonType.custom)
        leftItems.addTarget(self, action: #selector(StartQualityAssuranceVC.mailIcon(_:)), for: UIControlEvents.touchUpInside)
        leftItems.tag = 2
        leftItems.setImage(UIImage.init(named: "reload-icon"), for: UIControlState())
        leftItems.frame = CGRect.init(x: 0, y: 0, width: 22, height: 22)
        
        let leftbarButton = UIBarButtonItem.init(customView: leftItems)
        self.navigationItem.leftBarButtonItem = leftbarButton
        
        self.title = "Analysis Report"

    }
    func setReset() {
        
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = nil
        self.title = "Diagnosing"
        
     //   mailArray.removeAllObjects()
        print(mailArray)
    }
    
    func mailIcon(_ sender:UIButton)
    {
        print(sender.tag)
        if(sender.tag == 2)
        {
            //setItemsInCell(false)
            automateCurentIndexTest = 0
            setItemsInCell(true)
            setReset()
        }
        else{
            
            if mailArray.count > 0
            {
                //1. Create the alert controller.
                let alert = UIAlertController(title: "Email Address", message: "Enter your email address ", preferredStyle: .alert)
                
                //2. Add the text field. You can configure it however you need.
                alert.addTextField { (textField) in
                    textField.placeholder = "Enter your email address to send the result"
                }
                
                // 3. Grab the value from the text field, and print it when the user clicks OK.
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                    let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                    let txtLength = textField?.text?.count //.characters.count
                    if ( txtLength! <= 0)
                    {
                        self.showToast(message: "Please enter your eamil address in order to proceed!")
                    }
                    else{
                        
                        let isValudateemailAddress = self.isValidEmail(testStr: (textField?.text)!);
                        if isValudateemailAddress == false{
                            self.showToast(message: "Please enter valid email address!")
                        }
                        else
                        {
                            if BaseRequest.isConnectedToNetwork() == true
                            {
                                self.callAPIURL((textField?.text)!)
                            }
                            else
                            {
                                self.showToast(message: "Internet Connection not Available!")
                            }
                        }
                    }
                  //  print("Text field: \(textField?.text)")
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.showToast(message: "Kindly do some testing in order to send it to email!")
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: - Call API
    func callAPIURL(_ valuesAre:String)
    {
        var allStrign = "";
        for i in 0..<mailArray.count{
            let mainstrgin = mailArray[i]
            allStrign += mainstrgin as! String
        }
        
        let truncated = allStrign.substring(to: allStrign.index(before: allStrign.endIndex))

        print(truncated)

        let kBaseURL = "http://demo-appocta.com/eps/web.php?fun=send_mail&"+valuesAre+"&"+truncated
        let url:URL = URL(string: kBaseURL)!
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            guard let data = data, let _:URLResponse = response, error == nil else {
                print("error")
                return
            }
            
            let dataString =  String(data: data, encoding: String.Encoding.utf8)
            print(dataString ?? "")
            
            let alertController = UIAlertController(title:"", message: "Device results are send to your email address", preferredStyle: .alert)
            // Create the action.
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { action in
                NSLog("The simple alert's cancel action occured.")
            }
            
            // Add the action.
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
        task.resume()

    }

    // MARK: - QA Tests
    // MARK: - Carrier Name
    func getCarrierName() -> Bool
    {
        // For Carrier //
        
        // Setup the Network Info and create a CTCarrier object
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        if carrier != nil
        {
            // Get carrier name
            let carrierName = carrier!.carrierName
            if carrierName != nil{
                return true
            }
            else
            {
                return false
            }
        }
        else{
             return false
        }
        
        
        // For Carrier //
    }
    
    // MARK: - BSSID
     func fetchSSIDInfo() -> Bool {
        var currentSSID = ""
        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces) {
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
            
                if let unsafeInterfaceData = unsafeInterfaceData as? Dictionary<AnyHashable, Any> {
                    currentSSID =  (unsafeInterfaceData["SSID"] as? String)!
                }
            }
        }
        
        
        let ipAddress = getIPAddress()
        if(ipAddress == "" && currentSSID == "")
        {
            return false
        }
        else
        {
            return true
        }
    }
    

    
    // MARK: - PlaySound
    func playSound()
    {
        var audioPlayer:AVAudioPlayer!

        if Bundle.main.url(forResource: "SecondBeep", withExtension: "wav") != nil {
            do {
                audioPlayer =  try AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "SecondBeep", ofType: "mp3")!))
                audioPlayer.play()
                self.statusOfTest = true
                
            } catch {
                print("Error")
                self.statusOfTest = false
            }
        }
    }
    
    // MARK: - IP Address
    func getIPAddress() -> String{
        var address = ""
        let host = CFHostCreateWithName(nil,"www.google.com" as CFString).takeRetainedValue()
        CFHostStartInfoResolution(host, .addresses, nil)
        var success: DarwinBoolean = false
        if let addresses = CFHostGetAddressing(host, &success)?.takeUnretainedValue() as NSArray?,
            let theAddress = addresses.firstObject as? Data {
            var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
            if getnameinfo((theAddress as NSData).bytes.bindMemory(to: sockaddr.self, capacity: theAddress.count), socklen_t(theAddress.count),
                           &hostname, socklen_t(hostname.count), nil, 0, NI_NUMERICHOST) == 0 {
                if let numAddress = String(validatingUTF8: hostname) {
                    print(numAddress)
                    address = numAddress
                }
            }
        }
        return address
    }
    
    // MARK: - Blueetooth
    func getBlueetoth()
    {
        //Define class variable in your VC/AppDelegate
        var bluetoothPeripheralManager: CBPeripheralManager?
        
        //On viewDidLoad/didFinishLaunchingWithOptions
        let options = [CBCentralManagerOptionShowPowerAlertKey:0] //<-this is the magic bit!
        bluetoothPeripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: options)
        print(bluetoothPeripheralManager!)
    }
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        
        var statusMessage = ""
        
        switch peripheral.state {
        case .poweredOn:
            self.statusOfTest = true
            statusMessage = "Bluetooth Status: Turned On"
            break
            
        case .poweredOff:
            self.statusOfTest = true
            statusMessage = "Bluetooth Status: Turned Off"
            break
            
        case .resetting:
            self.statusOfTest = true
            statusMessage = "Bluetooth Status: Resetting"
            break
            
        case .unauthorized:
            self.statusOfTest = false
            statusMessage = "Bluetooth Status: Not Authorized"
            break
            
        case .unsupported:
            self.statusOfTest = false
            statusMessage = "Bluetooth Status: Not Supported"
            break
            
        default:
            statusMessage = "Bluetooth Status: Unknown"
            self.statusOfTest = false
            break
        }
        
        print(statusMessage)
       
    }
    
    // MARK: - Get Free size
    func getFreeSize() -> Int64? {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let dictionary = try? FileManager.default.attributesOfFileSystem(forPath: paths.last!) {
            if let freeSize = dictionary[FileAttributeKey.systemFreeSize] as? NSNumber {
                return freeSize.int64Value
            }
        }else{
            print("Error Obtaining System Memory Info:")
        }
        return nil
    }
    
    // MARK: - Get Total Size
    func getTotalSize() -> Int64?{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let dictionary = try? FileManager.default.attributesOfFileSystem(forPath: paths.last!) {
            if let freeSize = dictionary[FileAttributeKey.systemSize] as? NSNumber {
                return freeSize.int64Value
            }
        }else{
            print("Error Obtaining System Memory Info:")
        }
        return nil
    }
    
    // MARK: - Tourch Off
    func offTourchLight()
    {
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if device != nil
        {
            if (device?.hasTorch)! {
                do {
                    try device?.lockForConfiguration()
                    if (device?.torchMode == AVCaptureTorchMode.on)
                    {
                        device?.torchMode = AVCaptureTorchMode.off
                    } else {
                        try device?.setTorchModeOnWithLevel(1.0)
                    }
                    device?.unlockForConfiguration()
                } catch {
                    print(error)
                }
            }
            else
            {
            
            }
        }
        else{
            
        }

    }
    
    func toggleFlash() {
       
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if device != nil
        {
            if (device?.hasTorch)! {
                do {
                    try device?.lockForConfiguration()
                    if (device?.torchMode == AVCaptureTorchMode.on)
                    {
                        device?.torchMode = AVCaptureTorchMode.off
                        self.statusOfTest = true
                    } else {
                        try device?.setTorchModeOnWithLevel(1.0)
                        
                        self .perform(#selector(offTourchLight), with: nil, afterDelay: 0.7)
                    }
                    device?.unlockForConfiguration()
                } catch {
                    self.statusOfTest = false
                    print(error)
                }
            }
            else
            {
                let alertController = UIAlertController(title:"Alert", message: "No Flash light in the device", preferredStyle: .alert)
                // Create the action.
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { action in
                    NSLog("The simple alert's cancel action occured.")
                }
                
                // Add the action.
                alertController.addAction(cancelAction)
                
                //presentViewController(alertController, animated: true, completion: nil)
                
                self.statusOfTest = false
            }
        }
        else{
            self.statusOfTest = false
        
        }
    }
    
    // MARK: - Viberate
    func viberatePhone()
    {
        do{
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            self.statusOfTest = true
        }
        catch
        {
            self.statusOfTest = false
        }
        
        if(self.timer.isValid)
        {
            self.timer.invalidate()
        }
        
        self.getInputFromUser(testingResult: kviberaten)
    }
    
    // MARK: - Set Brightness
    func setBrightness()
    {
        do
        {
            UIScreen.main.brightness = CGFloat(1.0)
            self .perform(#selector(setNormalBrigtness), with: nil, afterDelay: 0.7)
            self.statusOfTest = true
        }
        catch
        {
            self.statusOfTest = false
        }
    }
    
    func setNormalBrigtness()
    {
        do
        {
            UIScreen.main.brightness = CGFloat(0.5)
        }
        catch
        {
        }
    }
    
    // MARK: - Get Location
    func getLocation() {
        guard CLLocationManager.locationServicesEnabled() else {
            print("Location services are disabled on your device. In order to use this app, go to " +
                "Settings → Privacy → Location Services and turn location services on.")
            return
        }
        
        let authStatus = CLLocationManager.authorizationStatus()
        guard authStatus == .authorizedWhenInUse else {
            switch authStatus {
            case .denied, .restricted:
                print("This app is not authorized to use your location. In order to use this app, " +
                    "go to Settings → GeoExample → Location and select the \"While Using " +
                    "the App\" setting.")
                
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
                
            default:
                print("Oops! Shouldn't have come this far.")
            }
            
            return
        }
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - CLLocationManagerDelegate methods
    
    // This is called if:
    // - the location manager is updating, and
    // - it was able to get the user's location.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       // let newLocation = locations.last!
        self.statusOfTest = true
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude:  (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!)
        
        geoCoder.reverseGeocodeLocation(location)
        {
            (placemarks, error) -> Void in
            
            let placeArray = placemarks as [CLPlacemark]!
            
            // Place details
            var placeMark: CLPlacemark!
            if placeMark != nil{
                
                placeMark = placeArray?[0]

                let city = placeMark.addressDictionary?["City"] as? String

                // Country
                let country = placeMark.addressDictionary?["Country"] as? String
                
                let finalString = NSString(format:"%@, %@", city!, country!)
                
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(finalString, forKey: kSaveCountryName)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    // This is called if:
    // - the location manager is updating, and
    // - it WASN'T able to get the user's location.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      //  print("Error: \(error)")
        self.statusOfTest = false

    }
   func headsetPlugIn() //-> Bool
   {
        var isFoundHeadphone = false
        let routes = AVAudioSession.init().currentRoute
        for var desc in routes.outputs
        {
            print(desc)
            if(desc.portType == AVAudioSessionPortHeadphones)
            {
                isFoundHeadphone =  true
                saveUsrDefault(keyValue: kManualHeadphone, valueIs: "100%")
                break
            }
        }
    
        print(isFoundHeadphone)
       // return isFoundHeadphone
    }
    
    
    // MARK: - Record Audio
    
    func stopRecord()
    {
        self.audioRecorder!.stop()
        
        self.showToast(message: "Your recording is now playing!")

        playReocrdedFile()
    }
    
    func setUpAudioRecorder() {
        do {
            let baseString : String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!
            let pathComponents = [baseString, "sound.m4a"]
            let audioURL = NSURL.fileURL(withPathComponents: pathComponents)
            
            let session = AVAudioSession.sharedInstance()
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try session.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
            try session.setActive(true)
            
            var recordSettings = [String : AnyObject]()
            recordSettings[AVFormatIDKey] = Int(kAudioFormatMPEG4AAC) as AnyObject?
            recordSettings[AVSampleRateKey] = 44100.0 as AnyObject?
            recordSettings[AVNumberOfChannelsKey] = 2 as AnyObject?
            
            self.audioRecorder = try AVAudioRecorder(url: audioURL!, settings: recordSettings)
            self.audioRecorder!.isMeteringEnabled = true
            self.audioRecorder!.prepareToRecord()
            
            recordTapped()
        } catch (_) {
        }
        
    }
    
    func recordTapped() {
        if (self.audioRecorder?.isRecording == nil || !self.audioRecorder!.isRecording) {
            if (self.audioRecorder?.isRecording == nil) {
                setUpAudioRecorder()
            }
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                self.audioRecorder!.record()
            } catch (_) {}
        } else {
            self.audioRecorder!.stop()
        }
    }
    
    func playReocrdedFile()
    {
        do
        {
            if !audioRecorder.isRecording {
                self.audioPlayer = try! AVAudioPlayer(contentsOf: self.audioRecorder.url)
                self.audioPlayer.prepareToPlay()
                self.audioPlayer.delegate = self
                self.audioPlayer.play()
            }
        }
        catch {
            
        }
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
        
        self.getRecordings(testingResult: kMicrophone)

    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
        self.getRecordings(testingResult: kMicrophone)
    }
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
        self.getRecordings(testingResult: kMicrophone)
    }
    
    func getRecordings(testingResult:String)
    {
        let message = "Is "+testingResult + " is working fine ?"
        // Create the alert controller
        let alertController = UIAlertController(title: "Testing complete", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.statusOfTest = true
                        self.setTest()
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            self.statusOfTest = false
            
                        self.setTest()
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setTest()
    {
        self.colelctionViewobject.animateProgressView()
        
        let curnetIndx = String(selectedIndex)
        if(currentManualStatus.contains(curnetIndx))
        {
            currentManualStatus.insert(curnetIndx, at: selectedIndex)
        }
        
        saveUsrDefault(keyValue: kAutomateTest, valueIs: "-1")

        
        let vw = arrayOfLines[audioRecordingIndex]
        vw.isHidden = false
        
        let currentOBject = arrayOfDiagonocs[audioRecordingIndex]
        let image: UIImage = UIImage(named: currentOBject.selectedImg)!
        let imgIcon = arrayOfImages[audioRecordingIndex]
        
        let progessBar = arrayAutomateTst[audioRecordingIndex]
        let lablelTxt = arraylabelOfCirularView[audioRecordingIndex]
        let changeColorOfBg = arrayOfLines[audioRecordingIndex]
        
        var ObjectsAre = ""
        var message = ""
        
        if(self.statusOfTest)
        {
            progessBar.isHidden = false
            imgIcon.image = image
            
            UserDefaults.standard.synchronize()
            
            if(currentOBject.titleOfItem == kGPS)
            {
                let locationName = UserDefaults.standard.object(forKey: kSaveCountryName)
                
                if locationName  == nil
                {
                    message = "App can't get your location"
                    lablelTxt.text = "0%"
                    ObjectsAre = currentOBject.titleOfItem + "="+"0&"
                    changeColorOfBg.backgroundColor = UIColor(netHex: 0xd92b2b)
                    progessBar.hideProgressView()
                    saveUsrDefault(keyValue: kAutoGPS, valueIs: "0%")
                }
                else
                {
                    lablelTxt.text = "100%"
                    ObjectsAre = currentOBject.titleOfItem + "="+"1&"
                    
                    let locationName = UserDefaults.standard.object(forKey: kSaveCountryName) as! NSString
                    
                    changeColorOfBg.backgroundColor = UIColor(netHex: 0x1dca66)
                    saveUsrDefault(keyValue: kAutoGPS, valueIs: "100%")
                    message = NSString(format:"Your current location is %@", locationName) as String
                }
            }
            else{
                message = currentOBject.titleOfItem + " is working fine"
                
                lablelTxt.text = "100%"
                ObjectsAre = currentOBject.titleOfItem + "="+"1&"
                
                changeColorOfBg.backgroundColor = UIColor(netHex: 0x1dca66)
            }
        }
        else{
            lablelTxt.text = "0%"
            progessBar.isHidden = false
            progessBar.hideProgressView()
            changeColorOfBg.backgroundColor = UIColor(netHex: 0xd92b2b)
            
            ObjectsAre = currentOBject.titleOfItem + "="+"0&"
            
            message = currentOBject.titleOfItem + " is not working fine"
        }
        
        if(currentOBject.titleOfItem == "Results")
        {
            progessBar.hideProgressView()
            lablelTxt.text = ""
            progessBar.isHidden = true
        }
        
        self.setObjectValue(valueStr: currentOBject.titleOfItem)
        
        showToast(message: message)
        mailArray.add(ObjectsAre)
        
        isRunning = false
    }
    
    // MARK: AVSystemPlayer - Notifications
    func listenVolumeButton()
    {
        let audioSession = AVAudioSession()
        do {
            try audioSession.setActive(true)
        } catch {
            print("some error")
        }
        audioSession.addObserver(self, forKeyPath: "outputVolume", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "outputVolume"
        {
            print("got in here")
            
            guard let dict = change,
                let temp = dict[NSKeyValueChangeKey.newKey] as? Float
                , temp != 0.5 else { return }

            if(self.volumeValue != 0.0)
            {
                if(self.volumeValue < temp)
                {
                    print("Volume Up")
                    saveUsrDefault(keyValue: kManualVolumeUp, valueIs: "100%")
                }
                else
                {
                    saveUsrDefault(keyValue: kManualVolumeDown, valueIs: "100%")
                    print("Volume Down")
                }
                
                self.volumeValue = temp
            }
            else
            {
                self.volumeValue = temp
            }
            
//            if(temp > 0.5) // volume up
//            {
//
//            }
//            else if(temp < 0.5) // volume down
//            {
//
//            }
        }
    }
    
    
    // MARK: - Button Action
    func loadResult(isShow: Bool)
    {
        let viewController = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "RestultViewController") as! RestultViewController
        
        if(isShow == true)
        {
            viewController.delegateObject = self
        }
        let nagiC = UINavigationController(rootViewController: viewController)
        nagiC.navigationBar.barTintColor = UIColor.init(netHex: 0x469ee5)
        nagiC.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: kFontName, size: CGFloat(kFontSizeTitle))!,  NSForegroundColorAttributeName: UIColor.white]
        
        self.present(nagiC, animated: true, completion: nil)
    }
    
    func callController()
    {
        let alertController = UIAlertController(title: "", message: "Do you want to test it again?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            self.loadAutoMaticOrManual(isAutoMti: true)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            self.segmentBar?.selectedSegmentIndex = 1
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
        
