//
//  CellRowsTableViewCell.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/31/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit

class CellRowsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblResultName: UILabel?
    @IBOutlet weak var lblResultValue: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
