//
//  ColorViewController.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/25/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController, UIScrollViewDelegate {

    var vwViews = [UIView]()
    
    @IBOutlet weak var scrolView: UIScrollView?
    
    var proggressValue = 0
    
    let totalCounter = 5
    
    var currentValue = 0
    
    let singtl = SingletonObject.sharedInstance
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        for  i in stride(from: 0, to: totalCounter, by: 1) {
            
            var frame = CGRect.zero
            frame.origin.x = (self.scrolView?.frame.size.width)! * CGFloat(i)
            frame.origin.y = 0
            frame.size = (self.scrolView?.frame.size)!
            
            let customView = ColorTabView(frame: frame)
            switch i
            {
            case 0:
                customView.vws?.backgroundColor = UIColor.red
                customView.lblTxt?.text = "Is this color is Red ??"
                break;
            case 1:
                customView.vws?.backgroundColor = UIColor.green
                customView.lblTxt?.text = "Is this color is green ??"
                break;
            case 2:
                customView.vws?.backgroundColor = UIColor.blue
                customView.lblTxt?.text = "Is this color is Blue ??"
                break;
            case 3:
                customView.vws?.backgroundColor = UIColor.gray
                customView.lblTxt?.text = "Is this color is Gray ??"
                break;
            case 4:
                customView.vws?.backgroundColor = UIColor.black
                customView.lblTxt?.text = "Is this color is Black ??"
                break;
            default:
                break
            }
            
            customView.btnFail?.tag = i
            customView.btnFail?.addTarget(self, action: #selector(failTest(sender:)), for: .touchUpInside)

            customView.btnPass?.tag = i
            customView.btnPass?.addTarget(self, action: #selector(passTest(sender:)), for: .touchUpInside)

            customView.lblTxt?.textColor = UIColor.white
            self.scrolView?.addSubview(customView)
            
            self.vwViews.append(customView)
        }
        

        
        self.scrolView?.isPagingEnabled = true
        self.scrolView?.contentSize = CGSize(width: (self.scrolView?.frame.size.width)! * CGFloat(totalCounter), height: (self.scrolView?.frame.size.height)!)
        
       // pageControl.addTarget(self, action: Selector(("changePage:")), for: UIControlEvents.valueChanged)
    }
    
    func failTest(sender: UIButton)
    {
        if(self.proggressValue <= 0)
        {
            self.proggressValue = 0
        }
        else{
            self.proggressValue-=1
        }
        
        self.currentValue+=1
        swictTonextview()
    }
    
    func refreshData()
    {
        let progessValue = CGFloat(self.proggressValue)
        let totalValue = CGFloat(self.totalCounter)
        
        let percentage = CGFloat(progessValue/totalValue)*100
        if(percentage > 90)
        {
            saveUsrDefault(keyValue: kColorValue, valueIs: "1")
            saveUsrDefault(keyValue: kManualScreenBrightness, valueIs: "100%")
        }
        else{
            saveUsrDefault(keyValue: kColorValue, valueIs: "0")
            saveUsrDefault(keyValue: kManualScreenBrightness, valueIs: "0%")
        }

        self.singtl.isFillData = true

        self.navigationController?.popViewController(animated: true)
    }

    func passTest(sender: UIButton)
    {
        if(self.currentValue >= totalCounter)
        {
            self.proggressValue = totalCounter
        }
        else
        {
            self.proggressValue+=1
        }
        
        self.currentValue+=1
        swictTonextview()
    }
    
    func swictTonextview()
    {
        if(self.currentValue < 5)
        {
            let x = CGFloat(self.currentValue)
            let w = self.scrolView?.bounds.size.width
            let currentPage = x * w!
  
            self.scrolView?.setContentOffset(CGPoint(x: currentPage,y :0), animated: true)
        }
        
        if(self.currentValue == 5)
        {
            self.refreshData()
        }
        print(self.currentValue)
    }
    
    func changePage(sender: AnyObject) -> () {
//        let x = CGFloat(pageControl.currentPage) * self.scrolView?.frame.size.width
//        self.scrolView?.setContentOffset(CGPoint(x: x,y :0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
//        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        //pageControl.currentPage = Int(pageNumber)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrolView?.contentOffset = CGPoint.zero
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
