//
//  MultipleTouchViewController.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/22/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit

class MultipleTouchViewController: UIViewController {

    @IBOutlet weak var vwTouches: UIView?
    
    let single = SingletonObject.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()

        self.vwTouches?.isMultipleTouchEnabled = true
        // Do any additional setup after loading the view.
        
        self .perform(#selector(CheckAndStatus), with: nil, afterDelay: 1.5)
    
            let mailItem = UIButton.init(type: UIButtonType.custom)
            
            mailItem.setImage(UIImage.init(named: "backs"), for: UIControlState())
            mailItem.frame = CGRect.init(x: 0, y: 0, width: 25, height: 20)
            mailItem.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
            mailItem.tag = 1
            
            let barButton = UIBarButtonItem.init(customView: mailItem)
            self.navigationItem.leftBarButtonItem = barButton
            
            self.title = "Multi Touch"
            // Do any additional setup after loading the view.
        }
        
        func back()
        {
            self.single.isFillData = true
            self.navigationController?.popViewController(animated: true)
        }
    
    func CheckAndStatus()
    {
        let testingResult = "Multiple Touch"
        let message = "Is "+testingResult + " is working fine ?"
        // Create the alert controller
        let alertController = UIAlertController(title: "Testing complete", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            saveUsrDefault(keyValue: kMultipleTouch, valueIs: "1")
            self.navigationController?.popViewController(animated: true)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            
            saveUsrDefault(keyValue: kMultipleTouch, valueIs: "0")
            self.navigationController?.popViewController(animated: true)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - Touch Began
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            print(touches)
            
            if let touch = touches.first {
                let currentPoint = touch.location(in: self.vwTouches)
                // do something with your currentPoint
                
                if(touches.count >= 1 &&  touches.count <= 3)
                {
                    let dots = UIView(frame: CGRect(x: currentPoint.x, y: currentPoint.y, width: 10, height: 10))
                    if(touches.count == 1)
                    {
                        dots.tag = 1
                        dots.backgroundColor = UIColor.red
                    }
                    else if(touches.count == 2)
                    {
                        dots.tag = 2
                        dots.backgroundColor = UIColor.blue
                    }
                    else if(touches.count == 3)
                    {
                        dots.tag = 3
                        dots.backgroundColor = UIColor.green
                    }

                    self.vwTouches?.addSubview(dots)
                }
            }
            
            
        }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first
        {
            let currentPoint = touch.location(in: self.vwTouches)
            
            
            for i in 0 ..< touches.count
            {

                let countr = i+1
                let viewTag = touch.view?.viewWithTag(countr)
                viewTag?.frame = CGRect(x: currentPoint.x, y: currentPoint.y, width: (viewTag?.frame.size.width)!, height: (viewTag?.frame.size.height)!)
                
                
                //let objectView = touches
            }
            
            // do something with your currentPoint
            
//            let dots = UIView(frame: CGRect(x: currentPoint.x, y: currentPoint.y, width: 10, height: 10))
//            dots.backgroundColor = UIColor.red
//            dots.tag = 3
//
//
//            if let viewWithTag = self.vwTouches?.viewWithTag(3)
//            {
//                viewWithTag.removeFromSuperview()
//            }
//            self.vwTouches?.addSubview(dots)
            
            
        }
    }
    
    //    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        if let touch = touches.first {
    //            let currentPoint = touch.location(in: self.vwTouches)
    //            // do something with your currentPoint
    //        }
    //    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
