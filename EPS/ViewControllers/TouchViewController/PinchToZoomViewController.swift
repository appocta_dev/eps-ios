//
//  PinchToZoomViewController.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/22/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit

class PinchToZoomViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var imageView:UIImageView!
    
    let single = SingletonObject.sharedInstance
    override func viewDidLoad() {
        
        super.viewDidLoad()
        scrollView.delegate = self
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0//maximum zoom scale you want
        scrollView.zoomScale = 1.0
        
        self .perform(#selector(CheckAndStatus), with: nil, afterDelay: 1.5)
   
        let mailItem = UIButton.init(type: UIButtonType.custom)
        
        mailItem.setImage(UIImage.init(named: "backs"), for: UIControlState())
        mailItem.frame = CGRect.init(x: 0, y: 0, width: 25, height: 20)
        mailItem.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        mailItem.tag = 1
        
        let barButton = UIBarButtonItem.init(customView: mailItem)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.title = "Pinch TO Zoom"
        // Do any additional setup after loading the view.
    }
    
    func back()
    {
        self.single.isFillData = true
        self.navigationController?.popViewController(animated: true)
    }
    
    func CheckAndStatus()
    {
        let testingResult = "Pinch To Zoom"
        let message = "Is "+testingResult + " is working fine ?"
        // Create the alert controller
        let alertController = UIAlertController(title: "Testing complete", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            saveUsrDefault(keyValue: kPinchToZoom, valueIs: "1")
            self.navigationController?.popViewController(animated: true)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            
            saveUsrDefault(keyValue: kPinchToZoom, valueIs: "0")
            self.navigationController?.popViewController(animated: true)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func actionButtonFail()
    {
        saveUsrDefault(keyValue: kPinchToZoom, valueIs: "0")
        self.navigationController?.popViewController(animated: true)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return imageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
