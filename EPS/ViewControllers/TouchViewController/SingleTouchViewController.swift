//
//  SingleTouchViewController.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/22/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit

class SingleTouchViewController: UIViewController {

    @IBOutlet weak var btnClick1: UIButton?
    @IBOutlet weak var btnClick2: UIButton?
    @IBOutlet weak var btnClick3: UIButton?
    
    @IBOutlet weak var btnClick4: UIButton?
    @IBOutlet weak var btnClick5: UIButton?
    @IBOutlet weak var btnClick6: UIButton?
    
    @IBOutlet weak var btnClick7: UIButton?
    @IBOutlet weak var btnClick8: UIButton?
    @IBOutlet weak var btnClick9: UIButton?
    
    let singtl = SingletonObject.sharedInstance
    var isAllTaps = 0
    
    @IBAction func buttonClick(sender: UIButton)
    {
        switch sender.tag {
        case 1:
            isAllTaps+=1
            self.btnClick1?.isEnabled = false
            break
        case 2:
            isAllTaps+=1
            self.btnClick2?.isEnabled = false
            break
        case 3:
            isAllTaps+=1
            self.btnClick3?.isEnabled = false
            break
        case 4:
            isAllTaps+=1
            self.btnClick4?.isEnabled = false
            break
        case 5:
            isAllTaps+=1
            self.btnClick5?.isEnabled = false
            break
        case 6:
            isAllTaps+=1
            self.btnClick6?.isEnabled = false
            break
        case 7:
            isAllTaps+=1
            self.btnClick7?.isEnabled = false
            break
        case 8:
            isAllTaps+=1
            self.btnClick8?.isEnabled = false
            break
        case 9:
            isAllTaps+=1
            self.btnClick9?.isEnabled = false
            break
        default:
            break
        }
        
        sender.setTitleColor(UIColor.white, for: .disabled)
        sender.setTitleColor(UIColor.white, for: .highlighted)
        sender.setTitleColor(UIColor.white, for: .focused)
        
        sender.backgroundColor = UIColor.gray
        
        if(isAllTaps == 9)
        {
            saveUsrDefault(keyValue: kSingleTouch, valueIs: "1")
            saveUsrDefault(keyValue: kManualTouch, valueIs: "100%")

            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    @IBAction func submitData()
    {
        self.singtl.isFillData = true
        saveUsrDefault(keyValue: kSingleTouch, valueIs: "0")
        saveUsrDefault(keyValue: kManualTouch, valueIs: "0%")
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let mailItem = UIButton.init(type: UIButtonType.custom)
        
        mailItem.setImage(UIImage.init(named: "backs"), for: UIControlState())
        mailItem.frame = CGRect.init(x: 0, y: 0, width: 25, height: 20)
        mailItem.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        mailItem.tag = 1
        
        let barButton = UIBarButtonItem.init(customView: mailItem)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.title = "Touch"
        // Do any additional setup after loading the view.
    }
    
    func back()
    {
        self.singtl.isFillData = true
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
