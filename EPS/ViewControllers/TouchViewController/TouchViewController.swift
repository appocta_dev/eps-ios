//
//  TouchViewController.swift
//  XDiag_Mobile
//
//  Created by MacBook on 1/22/18.
//  Copyright © 2018 digitonics. All rights reserved.
//

import UIKit

class TouchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var arrayOfDiagonocs = [diagnocsDM]()

    @IBOutlet weak var collectionViewCell: UICollectionView?
    let single = SingletonObject.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(buttonNotifition),
            name: NSNotification.Name(rawValue: kAutomateTestDialog),
            object: nil)
        
        let mailItem = UIButton.init(type: UIButtonType.custom)
        
        mailItem.setImage(UIImage.init(named: "backs"), for: UIControlState())
        mailItem.frame = CGRect.init(x: 0, y: 0, width: 25, height: 20)
        mailItem.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        mailItem.tag = 1
        
        self.title = "Touch"
        let barButton = UIBarButtonItem.init(customView: mailItem)
        self.navigationItem.leftBarButtonItem = barButton
        // Do any additional setup after loading the view.
    }
    
    
    func back()
    {
        self.single.isFillData = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionButtonGoBack()
    {
        back()
    }
    
    @objc func buttonNotifition(_ notification: Notification)
    {
        for i in 0 ..< self.single.arrayOfObjects.count
        {
            let indxValue = self.single.arrayOfObjects[i]
            
            let cellObject = self.single.cellOfObjects[i]
            if(indxValue.percentage == "100")
            {
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0xd92b2b)
                cellObject.circularview!.layer.borderColor = UIColor(netHex:0x1dca66).cgColor
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0x1dca66)
                
                cellObject.circularPercentage?.text = "100%"
            }
            else
            {
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0xd92b2b)
                
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0xd92b2b)
                cellObject.circularview!.layer.borderColor = UIColor(netHex:0xd92b2b).cgColor
                cellObject.circularview?.backgroundColor = UIColor(netHex: 0xd92b2b)
                cellObject.collectionAnimated?.hideProgressView()
                
                cellObject.circularPercentage?.text = "0%"
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
       
        self.arrayOfDiagonocs.removeAll()
        
        self.single.arrayOfObjects.removeAll()
        self.single.cellOfObjects.removeAll()

        
        for i in 0 ..< 3
        {
            var imgName = ""
            var titleName = ""
            var isSelected = false
            var percentage = "0"
            var slectedInd = -1
            var selectedImg = ""
            
            switch i
            {
            case 0:
                let objectValue = getValueForKey(keyValue: kSingleTouch)
                if(objectValue == "1")
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "100"
                }
                else if(objectValue == "-1")
                {
                    isSelected = false
                    percentage = "-1"
                    percentage = "0"
                }
                else
                {
                    slectedInd = i
                    isSelected  = true
                    percentage = "0"
                }
                imgName = "Touch-Screen"
                titleName = "Single Touch"
                selectedImg = "Touch-ScreenOver"
                break
            case 1:
                imgName = "multitouch_off"
                titleName = "Multiple Touch"
                selectedImg = "multitouch_on"
                let objectValue = getValueForKey(keyValue: kMultipleTouch)
                if(objectValue == "1")
                {
                    isSelected = true
                    slectedInd = i
                    percentage = "100"
                }
                else if(objectValue == "-1")
                {
                    isSelected = false
                    percentage = "-1"
                    percentage = "0"
                }
                else
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "0"
                }
                
                break
            case 2:
                imgName = "pinchtozoom_off"
                titleName = "Pinch Zoom"
                selectedImg = "pinchtozoom_on"
                
                let objectValue = getValueForKey(keyValue: kPinchToZoom)
                if(objectValue == "1")
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "100"
                }
                else if(objectValue == "-1")
                {
                    isSelected = false
                    percentage = "-1"
                    percentage = "0"
                }
                else
                {
                    slectedInd = i
                    isSelected = true
                    percentage = "0"
                }
                break
                
            default:
                break
            }
            
            let mainOBject = diagnocsDM(title: titleName, imgNames: imgName, isSelced: isSelected, percentage: percentage, indexValue: slectedInd,selectedImg:selectedImg)
            self.arrayOfDiagonocs.insert(mainOBject!, at: i)
        }
        
        self.collectionViewCell?.delegate = self
        self.collectionViewCell?.dataSource = self
        self.collectionViewCell?.reloadData()
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiagnocItem", for: indexPath) as! DiagnocItem
        
        cell.collectionCellImg!.layer.borderWidth = 1
        cell.collectionCellImg!.layer.masksToBounds = false
        cell.collectionCellImg!.layer.borderColor = UIColor(netHex:0x363a42).cgColor
        cell.collectionCellImg!.layer.cornerRadius = cell.collectionCellImg!.frame.height/2
        cell.collectionCellImg!.clipsToBounds = true
        
        let selectedColor = UIColor(netHex:0x349efb)
        cell.collectionCellImgInner!.layer.borderWidth = 1
        cell.collectionCellImgInner!.layer.masksToBounds = false
        cell.collectionCellImgInner!.layer.borderColor = UIColor(netHex:0x363a42).cgColor
        cell.collectionCellImgInner!.layer.cornerRadius = cell.collectionCellImgInner!.frame.height/2
        cell.collectionCellImgInner!.clipsToBounds = true
        cell.collectionCellImgInner?.backgroundColor = selectedColor
        
        let colorBar = UIColor(netHex:0xc6cbd8)
        cell.collectionCellTitle?.textColor = colorBar
        
        let currentOBject = arrayOfDiagonocs[indexPath.row]
        
        if(currentOBject.isSelected && currentOBject.selectedIndex == indexPath.row)
        {
            cell.circularview?.isHidden = false
            saveUsrDefault(keyValue: kAutomateTest, valueIs: "1")
            cell.collectionAnimated?.isHidden = false
            cell.collectionAnimated?.animateProgressView()
        }
        else{
            cell.circularview?.isHidden = true
            cell.collectionAnimated?.hideProgressView()
        }
        
        cell.circularPercentage?.text = currentOBject.percentage
        cell.collectionCellImgInner?.isHidden = true
        
        let image: UIImage = UIImage(named: currentOBject.imgName)!
        
        cell.collectionCellTitle?.text = currentOBject.titleOfItem
        cell.collectionCellIcon?.image = image
        
        cell.circularview!.layer.borderWidth = 1
        cell.circularview!.layer.masksToBounds = false
        
        cell.circularview!.layer.cornerRadius = cell.circularview!.frame.height/2
        cell.circularview!.clipsToBounds = true

        self.single.arrayOfObjects.append(currentOBject)
        self.single.cellOfObjects.append(cell)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var viewController = UIViewController()
        
        
        let currentOBject = arrayOfDiagonocs[indexPath.row]
        if(currentOBject.titleOfItem == "Single Touch")
        {
            viewController = UIStoryboard(name:"touchSBoard", bundle:nil).instantiateViewController(withIdentifier: "SingleTouchViewController") as! SingleTouchViewController
        }
        else if(currentOBject.titleOfItem == "Multiple Touch")
        {
            viewController = UIStoryboard(name:"touchSBoard", bundle:nil).instantiateViewController(withIdentifier: "MultipleTouchViewController") as! MultipleTouchViewController
        }
        else
        {
            viewController = UIStoryboard(name:"touchSBoard", bundle:nil).instantiateViewController(withIdentifier: "PinchToZoomViewController") as! PinchToZoomViewController
        }
     
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayOfDiagonocs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
